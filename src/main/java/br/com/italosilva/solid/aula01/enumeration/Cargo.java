package br.com.italosilva.solid.aula01.enumeration;

import br.com.italosilva.solid.aula01.rules.CalculoSalario;
import br.com.italosilva.solid.aula01.rules.DezOuVintePorCento;
import br.com.italosilva.solid.aula01.rules.QuinzeOuVintePorCento;

public enum Cargo {

    DESENVOLVEDOR(new DezOuVintePorCento()),
    DBA(new QuinzeOuVintePorCento()),
    TESTER(new QuinzeOuVintePorCento());

    private final CalculoSalario calculoSalario;

    Cargo(CalculoSalario calculoSalario) {
        this.calculoSalario = calculoSalario;
    }

    public CalculoSalario getCalculoSalario() {
        return calculoSalario;
    }
}
