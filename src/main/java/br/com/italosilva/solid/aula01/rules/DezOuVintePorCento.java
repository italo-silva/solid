package br.com.italosilva.solid.aula01.rules;

import br.com.italosilva.solid.aula01.domain.Funcionario;

public class DezOuVintePorCento implements CalculoSalario {

    @Override
    public double calcula(Funcionario funcionario) {
        if(funcionario.getSalarioBase() > 3000.0) {
            return funcionario.getSalarioBase() * 0.8;
        }
        else {
            return funcionario.getSalarioBase() * 0.9;
        }
    }
}
