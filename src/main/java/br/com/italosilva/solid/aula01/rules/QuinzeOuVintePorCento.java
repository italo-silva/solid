package br.com.italosilva.solid.aula01.rules;

import br.com.italosilva.solid.aula01.domain.Funcionario;

public class QuinzeOuVintePorCento implements CalculoSalario {

    @Override
    public double calcula(Funcionario funcionario) {
        if(funcionario.getSalarioBase() > 2000.0) {
            return funcionario.getSalarioBase() * 0.75;
        }
        else {
            return funcionario.getSalarioBase() * 0.85;
        }
    }
}
