package br.com.italosilva.solid.aula01.rules;

import br.com.italosilva.solid.aula01.domain.Funcionario;

public interface CalculoSalario {

    public double calcula(Funcionario funcionario);
}
