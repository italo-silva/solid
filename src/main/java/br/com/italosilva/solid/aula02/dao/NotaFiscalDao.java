package br.com.italosilva.solid.aula02.dao;

import br.com.italosilva.solid.aula02.domain.NotaFiscal;
import br.com.italosilva.solid.aula02.rules.AcoesAposCriacaoNotaFiscal;

public class NotaFiscalDao implements AcoesAposCriacaoNotaFiscal {

    @Override
    public void executa(NotaFiscal notaFiscal) {

        System.out.println("salva nf no banco");
    }
}
