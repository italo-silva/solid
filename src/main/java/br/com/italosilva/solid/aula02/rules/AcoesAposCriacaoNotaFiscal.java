package br.com.italosilva.solid.aula02.rules;

import br.com.italosilva.solid.aula02.domain.NotaFiscal;

public interface AcoesAposCriacaoNotaFiscal {

    public void executa(NotaFiscal notaFiscal);
}
