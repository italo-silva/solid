package br.com.italosilva.solid.aula02.service;

import br.com.italosilva.solid.aula02.domain.NotaFiscal;
import br.com.italosilva.solid.aula02.rules.AcoesAposCriacaoNotaFiscal;

public class EnviadorDeEmail implements AcoesAposCriacaoNotaFiscal {

    @Override
    public void executa(NotaFiscal notaFiscal) {
        System.out.println("envia email da nf " + notaFiscal.getId());
    }
}
