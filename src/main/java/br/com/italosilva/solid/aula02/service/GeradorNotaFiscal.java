package br.com.italosilva.solid.aula02.service;

import br.com.italosilva.solid.aula02.domain.Fatura;
import br.com.italosilva.solid.aula02.domain.NotaFiscal;
import br.com.italosilva.solid.aula02.rules.AcoesAposCriacaoNotaFiscal;

import java.util.List;

public class GeradorNotaFiscal {

    private final List<AcoesAposCriacaoNotaFiscal> acoes;

    public GeradorNotaFiscal(List<AcoesAposCriacaoNotaFiscal> acoes) {
        this.acoes = acoes;
    }

    public NotaFiscal gera(Fatura fatura) {

        double valor = fatura.getValorMensal();

        NotaFiscal nf = new NotaFiscal(valor, impostoSimplesSobreO(valor));

        for (AcoesAposCriacaoNotaFiscal acao : this.acoes) {
            acao.executa(nf);
        }

        return nf;
    }

    private double impostoSimplesSobreO(double valor) {
        return valor * 0.06;
    }
}
