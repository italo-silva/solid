package br.com.italosilva.solid.aula04.domain;

public class Boleto {

    private double valor;

    public Boleto(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return valor;
    }
}
