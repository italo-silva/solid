package br.com.italosilva.solid.aula04.service;

import br.com.italosilva.solid.aula04.domain.Boleto;
import br.com.italosilva.solid.aula04.domain.Fatura;
import br.com.italosilva.solid.aula04.domain.Pagamento;
import br.com.italosilva.solid.aula04.enums.MeioDePagamento;

import java.util.List;

public class ProcessadorDeBoletos {

    public void processa(List<Boleto> boletos, Fatura fatura) {

        for(Boleto boleto : boletos) {
            Pagamento pagamento = new Pagamento(boleto.getValor(),
                    MeioDePagamento.BOLETO);
            fatura.adicionaPagamento(pagamento);
        }
    }
}
