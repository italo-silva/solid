package br.com.italosilva.solid.aula04.domain;

import br.com.italosilva.solid.aula04.enums.MeioDePagamento;

public class Pagamento {

    private final double valor;
    private final MeioDePagamento meioDePagamento;

    public Pagamento(double valor, MeioDePagamento meioDePagamento) {
        this.valor = valor;
        this.meioDePagamento = meioDePagamento;
    }

    public double getValor() {
        return valor;
    }
}
