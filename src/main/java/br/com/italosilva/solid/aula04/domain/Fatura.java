package br.com.italosilva.solid.aula04.domain;

import java.util.Collections;
import java.util.List;

public class Fatura {

    private List<Pagamento> pagamentos;

    private double valor;

    private boolean pago;

    public List<Pagamento> getPagamentos() {
        return Collections.unmodifiableList(pagamentos);
    }

    public void adicionaPagamento(Pagamento pagamento) {

        this.pagamentos.add(pagamento);

        if(valorTotalDosPagamentos() >= this.valor) {
            this.pago = true;
        }
    }

    private double valorTotalDosPagamentos() {
        double valor = 0.0;
        for (Pagamento pagamento : this.pagamentos) {
            valor += pagamento.getValor();
        }
        return valor;
    }

}
