package br.com.italosilva.solid.aula03.service;

import br.com.italosilva.solid.aula03.rules.TabelaDePreco;

public class TabelaPrecosPadrao implements TabelaDePreco {

    @Override
    public double descontoPara(double valor) {
        if(valor>5000) return 0.03;
        if(valor>1000) return 0.05;
        return 0;
    }
}
