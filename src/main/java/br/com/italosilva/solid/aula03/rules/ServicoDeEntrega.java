package br.com.italosilva.solid.aula03.rules;

public interface ServicoDeEntrega {

    double para(String cidade);
}
