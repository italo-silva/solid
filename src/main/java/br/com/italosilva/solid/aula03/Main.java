package br.com.italosilva.solid.aula03;

import br.com.italosilva.solid.aula03.domain.Compra;
import br.com.italosilva.solid.aula03.service.CalculadoraPrecos;
import br.com.italosilva.solid.aula03.service.Frete;
import br.com.italosilva.solid.aula03.service.TabelaPrecosPadrao;

public class Main {

    public static void main(String[] args) {

        CalculadoraPrecos calculadora = new CalculadoraPrecos(new TabelaPrecosPadrao(), new Frete());
        System.out.println(calculadora.calcula(new Compra(1001.0, "SAO PAULO")));

    }
}
