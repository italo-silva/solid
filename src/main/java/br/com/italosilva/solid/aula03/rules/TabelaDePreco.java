package br.com.italosilva.solid.aula03.rules;

public interface TabelaDePreco {

    double descontoPara(double valor);
}
