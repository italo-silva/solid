package br.com.italosilva.solid.aula03.service;

import br.com.italosilva.solid.aula03.rules.ServicoDeEntrega;

public class Frete implements ServicoDeEntrega {

    @Override
    public double para(String cidade) {
        if("SAO PAULO".equals(cidade.toUpperCase())) {
            return 15;
        }
        return 30;
    }
}
