package br.com.italosilva.solid.aula03.service;

import br.com.italosilva.solid.aula03.domain.Compra;
import br.com.italosilva.solid.aula03.rules.ServicoDeEntrega;
import br.com.italosilva.solid.aula03.rules.TabelaDePreco;

public class CalculadoraPrecos {

    private final TabelaDePreco tabela;
    private final ServicoDeEntrega entrega;

    public CalculadoraPrecos(TabelaDePreco tabela, ServicoDeEntrega entrega) {
        this.tabela = tabela;
        this.entrega = entrega;
    }

    public double calcula(Compra produto) {

        double desconto = tabela.descontoPara(produto.getValor());
        double frete = entrega.para(produto.getCidade());

        return produto.getValor() * (1-desconto) + frete;
    }
}
