package br.com.italosilva.solid.aula05.domain;

import br.com.italosilva.solid.aula05.exception.ContaNaoRendeException;

public class ContaEstudante extends ContaComum {

    private ManipuladorDeSaldo manipuladorDeSaldo;
    private int milhas;

    public ContaEstudante() {
        this.manipuladorDeSaldo = new ManipuladorDeSaldo();
    }

    public void deposita(double valor) {
        this.manipuladorDeSaldo.deposita(valor);
        this.milhas = (int) valor;
    }

    public int getMilhas() {
        return milhas;
    }
}
