package br.com.italosilva.solid.aula05.exception;

public class ContaNaoRendeException extends Throwable {

    public ContaNaoRendeException() {
        throw new RuntimeException("Conta não rende...");
    }
}
