package br.com.italosilva.solid.aula05;

import br.com.italosilva.solid.aula05.domain.ContaComum;

import java.util.Arrays;
import java.util.List;

public class ProcessadorInvestimentos {

    public static void main(String[] args) {

        for (ContaComum conta : contasDoBanco()) {
            conta.rende();
        }
    }

    private static List<ContaComum> contasDoBanco() {
        return Arrays.asList(new ContaComum());
    }
}
