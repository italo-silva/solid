package br.com.italosilva.solid.aula05.domain;

import br.com.italosilva.solid.aula05.exception.ContaNaoRendeException;

public class ContaComum {

    private ManipuladorDeSaldo manipuladorDeSaldo;

    public ContaComum() {
        this.manipuladorDeSaldo = new ManipuladorDeSaldo();
    }

    public void saca(double valor ){
        this.manipuladorDeSaldo.saca(valor);
    }

    public void deposita(double valor) {
        this.manipuladorDeSaldo.deposita(valor);
    }

    public double getSaldo() {
        return manipuladorDeSaldo.getSaldo();
    }

    public void rende() {
        this.manipuladorDeSaldo.rende(1*1);
    }
}
